﻿using System;
using System.Collections.Generic;
using FMG.GuardianIntegration;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FMG.GuardianIntegrationTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void PushToGuardianTest()
        {
            //var svc = new GuardianIntegrationService("123456",                         // pub id
            //                                         "paul scivetti",               // name
            //                                         "paul.scivetti@faulknermediagroup.com",       // email
            //                                         "www.google.com",              // site url
            //                                        false)                          // online
            //        .WithCustomId("7K")
            //        .WithProducts(new List<string>() {"fmgsites"})
            //    ;


            //var result = svc.PushToGuardian("create");


            var svc = new GuardianIntegrationService("337549",                         // pub id
                        "paul scivetti",               // name
                        "uxglic@glic.com",       // email
                        "www.google.com",              // site url
                        false)                          // online
                    .WithCustomId("7K")
                    .WithProducts(new List<string>() { "fmgsites" })
                ;


            var result = svc.PushToGuardian("create");


            Assert.AreEqual(true, result);

        }

        [TestMethod] 
        public void IsGuardianProgramTest()
        {
            // check all the programs that are supposed to be Guardian
            Assert.IsTrue(GuardianIntegrationService.IsGuardianProgram(99));
            Assert.IsTrue(GuardianIntegrationService.IsGuardianProgram(100));

            // and, just for fun, one that isn't
            Assert.IsFalse(GuardianIntegrationService.IsGuardianProgram(101));

        }
    }
}
