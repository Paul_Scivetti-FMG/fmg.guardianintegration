﻿using System;
using System.Collections.Generic;
using System.Linq;
using FMG.GuardianIntegration.Contracts;
using log4net;
using System.Reflection;
using IO.Swagger.GuardianIntegration.Api;
using IO.Swagger.GuardianIntegration.Model;


namespace FMG.GuardianIntegration
{
    public class GuardianIntegrationService : IGuardianIntegrationService
    {


        private static readonly Configuration Config = (Configuration)System.Configuration.ConfigurationManager.GetSection("FMG.GuardianIntegration.Configuration");

        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // Swagger API wrapper
        private DefaultApi _api;
        private DefaultApi Api
        {
            get { return _api ?? (_api = new DefaultApi(Config.EndpointUrl)); }
        }


        #region Properties and Fields


        private string PublicId { get; set; }

        private string CustomId { get; set; }

        private string SiteType { get; set; }

        private string Name { get; set; }

        private string Email { get; set; }

        private string Site { get; set; }

        private bool Online { get; set; }

        private string Region { get; set; }

        private List<string> Products { get; set; }



        #endregion Properties and Fields


        #region Constructor


        public GuardianIntegrationService()
        {

            // set credentials
            Api.Configuration.AddDefaultHeader("Authorization", Config.Authorization);
            Api.Configuration.AddDefaultHeader("UserID", Config.UserID);
            Api.Configuration.AddDefaultHeader("AppKey", Config.AppKey);
        }


        public GuardianIntegrationService(string publicId, string name, string email, string site, bool online)
        {

            // set credentials
            Api.Configuration.AddDefaultHeader("Authorization", Config.Authorization);
            Api.Configuration.AddDefaultHeader("UserID", Config.UserID);
            Api.Configuration.AddDefaultHeader("AppKey", Config.AppKey);

            // set required fields
            this.PublicId = publicId;
            this.Name = name;
            this.Email = email;
            this.Site = site;
            this.Online = online;
        }

        public void SetPublicId(string publicId)
        {
            this.PublicId = publicId;
        }

        public void SetName(string name)
        {
            this.Name = name;
        }
        public void SetEmail(string email)
        {
            this.Email = email;
        }

        public void SetSite(string site)
        {
            this.Site = site;
        }

        public void SetOnline(bool online)
        {
            this.Online = online;
        }


        #endregion

        #region static methods

        public static bool IsGuardianProgram(int pgm)
        {
            Log.DebugFormat("Checking to see if program {0} is Guardian", pgm);
            var ids = Config.GuardianProgramIds;
            Log.DebugFormat("Checking {1} IDs to see if program {0} is Guardian.  Contains? {2}", pgm, ids.Count, ids.Contains(pgm));

            return ids.Contains(pgm);
        }

        #endregion


        #region public methods


        // actions  - create or update
        public bool PushToGuardian(string action)
        {
            bool didItWork = false;

            try
            {

                var request = BuildRequest(action);

                var response = Api.HookAssignmentAlias(request);

                var logInfo = MakeLoggingString(request);

                Log.Info($"Pushed to Guardian {logInfo}, response {response.Status} {response.Message}");

                // did it work?
                didItWork = response.Status.HasValue && response.Status.Value == 200;
            }
            catch (Exception ex)
            {
                var prods = String.Join(", ", Products);
                Log.Error($"Error pushing info to Guardian public id: {PublicId} name: {Name}  error: {ex.Message}");
                if (ex.InnerException != null)
                {
                    // walk down the inner exceptions too
                    var i = 0;
                    while (ex.InnerException != null)
                    {
                        ex = ex.InnerException;
                        ++i;
                        Log.Error($"Guardian inner exception {i}, msg {ex.Message}");
                    }
                }
                Log.Error($"Guardian details: ext id: {CustomId} email: {Email}  online: {Online} site: {Site}, products: [{prods}]");
                throw;
            }

            return didItWork;
        }

        private Request BuildRequest(string action)
        {
            Request request = new Request();
            var d = new AssignmentAliaseshookData();

            // sanity check for missing params
            d.PublicId = IsRequired("PublicId");
            d.Name = IsRequired("Name");
            d.Email = IsRequired("Email");
            d.Site = IsRequired("Site");

            // convert from bool to int for api
            d.Online = Online;

            // optional params - fill in if present
            d.Region = IsOptional(Region, string.Empty);
            d.SiteType = IsOptional(SiteType, string.Empty);
            d.CustomId = IsOptional(CustomId, string.Empty);

            // product list
            d.Products = new List<string>();
            if (Products != null && Products.Any())
            {
                d.Products.AddRange(Products);
            }

            request.Data = d;
            request.Action = action;
            return request;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="action">update or create</param>
        /// <returns></returns>
        public ValidationResult Validate(string action)
        {
            action = action.ToLower().Trim();

            // make sure caller gave us a good action
            switch (action)
            {
                case "update":
                case "create":
                    action = "validate_" + action;
                    break;

                default:
                    throw new ArgumentException("invalid action");
            }


            var res = new ValidationResult();
            try
            {

                var request = BuildRequest(action);

                var response = Api.HookAssignmentAlias(request);

                var logInfo = MakeLoggingString(request);

                Log.Info($"Pushed to Guardian {logInfo}, response {response.Status} {response.Message}");

                // did it work?
                var didItWork = response.Status.HasValue && response.Status.Value == 200;

                res.Error = !didItWork;
                res.StatusCode = response.Status ?? 0;
                res.Message = response.Message;
            }
            catch (Exception ex)
            {
                res.Message = ex.Message;
                res.StatusCode = -1;
                res.Error = true;
            }

            return res;
        }

        public GuardianIntegrationService WithCustomId(string customId)
        {
            this.CustomId = customId;
            return this;
        }
        public GuardianIntegrationService WithRegion(string region)
        {
            this.Region = region;
            return this;
        }

        public GuardianIntegrationService WithSiteType(string siteType)
        {
            this.SiteType = siteType;
            return this;
        }

        public GuardianIntegrationService WithProducts(List<string> products)
        {
            this.Products = new List<string>();
            this.Products.AddRange(products);
            return this;
        }


        #endregion


        #region private helpers

        private string MakeLoggingString(Request rqst)
        {
            var str = $"Public id: {PublicId}  Extrn id: {CustomId} Name: {Name} Action: {rqst.Action}";
            return str;
        }


        private string IsRequired(string propName)
        {
            var propertyInfo = this.GetType().GetProperty(propName, BindingFlags.NonPublic | BindingFlags.Instance);
            if (propertyInfo != null)
            {
                var prop = propertyInfo.GetValue(this);
                var propStr = (prop ?? string.Empty).ToString();

                // sanity check for missing params
                if (string.IsNullOrWhiteSpace(propStr))
                    throw new Exception($"Required property {propName} is missing");

                return propStr;
            }

            throw new Exception($"Propert {propName} not found");
        }


        private string IsOptional(string prop, string dfltValue)
        {
            if (string.IsNullOrWhiteSpace(prop))
                return dfltValue;
            else
                return prop;
        }

        #endregion

        
    }
}
