﻿using System.Configuration;

namespace FMG.GuardianIntegration
{
    public class ConfigurationHandler : IConfigurationSectionHandler
  {
    public object Create(object parent, object configContext, System.Xml.XmlNode section)
    {
      var config = new Configuration();
      config.LoadValuesFromXml(section);

      return config;
    }
  }
}
