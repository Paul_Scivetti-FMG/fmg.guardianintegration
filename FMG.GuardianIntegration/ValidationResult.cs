﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMG.GuardianIntegration
{
    public class ValidationResult
    {
        public bool Error { get; set; }
        public int StatusCode{ get; set; }
        public string Message{ get; set; }
    }
}
