﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using FMG.Utilities;

namespace FMG.GuardianIntegration
{
    [XmlRoot("FMG.GuardianIntegration.Configuration")]
    public class Configuration
    {
        public string Authorization { get; set; }
        public string UserID { get; set; }
        public string AppKey { get; set; }
        public string EndpointUrl { get; set; }

        public string GuardianProgramIdsCsv { get; set; }

        public List<int> GuardianProgramIds { get; set; }

        internal void LoadValuesFromXml(XmlNode section)
        {
            if (section == null)
            {
                throw new ArgumentNullException("section", "The configuration section \"FMG.GuardianIntegration.Configuration\" cannot be null or has not been added to the app/web configuration file.");
            }

            var result = section.ToObject<Configuration>();
            this.Authorization = result.Authorization;
            this.UserID = result.UserID;
            this.AppKey = result.AppKey;
            this.EndpointUrl = result.EndpointUrl;

            string pgms = result.GuardianProgramIdsCsv;

            var ids = pgms.Split(',');
            this.GuardianProgramIds = new List<int>();
            foreach (var i in ids)
            {
                int v;
                if (int.TryParse(i, out v))
                    this.GuardianProgramIds.Add(v);
            }


        }
    }
}
