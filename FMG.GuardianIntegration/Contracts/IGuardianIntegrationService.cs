﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FMG.GuardianIntegration.Contracts
{
    public interface IGuardianIntegrationService
    {
        bool PushToGuardian(string action);
        ValidationResult Validate(string action);

        GuardianIntegrationService WithCustomId(string customId);

        GuardianIntegrationService WithRegion(string region);

        GuardianIntegrationService WithSiteType(string siteType);

        GuardianIntegrationService WithProducts(List<string> products);


        // setters to user when using default construtor
        void SetPublicId(string publicId);
        void SetName(string name);
        void SetEmail(string email);
        void SetSite(string site);
        void SetOnline(bool online);


    }
}
