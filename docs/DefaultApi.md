# IO.Swagger.Api.DefaultApi

All URIs are relative to *https://example.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**HookAssignmentAlias**](DefaultApi.md#hookassignmentalias) | **POST** /assignment_aliases/hook | 


<a name="hookassignmentalias"></a>
# **HookAssignmentAlias**
> InlineResponse200 HookAssignmentAlias (Request request)



Webhook endpoint for updating site / user information 

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class HookAssignmentAliasExample
    {
        public void main()
        {
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new DefaultApi();
            var request = new Request(); // Request | JSON object describing the package

            try
            {
                InlineResponse200 result = apiInstance.HookAssignmentAlias(request);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling DefaultApi.HookAssignmentAlias: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**Request**](Request.md)| JSON object describing the package | 

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

