# IO.Swagger.Model.InlineResponse200
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Status** | **int?** | return code | [optional] 
**Message** | **string** | message describing what happened | [optional] 
**Data** | [**AssignmentAliaseshookData**](AssignmentAliaseshookData.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

