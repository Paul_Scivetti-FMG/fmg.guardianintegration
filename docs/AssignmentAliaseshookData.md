# IO.Swagger.Model.AssignmentAliaseshookData
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**PublicId** | **string** | FMG ID | [optional] 
**CustomId** | **string** | if we provided a custom_id, this would be it | [optional] 
**SiteType** | **string** | agency or agent...describes who owns the site | [optional] 
**Name** | **string** | name of the contact | [optional] 
**Email** | **string** | contact email address | [optional] 
**Site** | **string** | URL for the website | [optional] 
**Online** | **bool?** | if the site is enabled | [optional] 
**Region** | **string** |  | [optional] 
**Products** | **List&lt;string&gt;** | products the user has purchased | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

